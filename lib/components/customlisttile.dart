import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../model/articles.dart';
import '../pages/fulldetails.dart';

Widget customlisttile(Articles article, BuildContext context) {
  return GestureDetector(
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => FullDetails(
              article: article,
            ),
          ));
    },
    child: Container(
      decoration: const BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
          color: Colors.pink,
          blurRadius: 5.0,
        ),
      ]),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.all(14),
            padding: EdgeInsets.all(5),
            height: 200,
            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(article.urlToImage), fit: BoxFit.cover),
              borderRadius: BorderRadius.circular(12.0),
            ),
          ),
          SizedBox(
            height: 8.0,
          ),
          Container(
            margin: EdgeInsets.all(14),
            padding: EdgeInsets.all(5),
            width: double.infinity,
            child: Text(article.title,
            style: TextStyle(
              fontSize: 20
            ),),
          ),
        ],
      ),
    ),
  );
}
