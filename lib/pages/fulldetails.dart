import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../model/articles.dart';

class FullDetails extends StatelessWidget {
  final Articles article;

  FullDetails({
    required this.article
});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(article.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.all(14),
            padding: EdgeInsets.all(5),
            height: 200,
            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(article.urlToImage), fit: BoxFit.cover),
              borderRadius: BorderRadius.circular(12.0),
            ),
          ),
          Container(
            height: 30,
            width: double.infinity,
            margin: EdgeInsets.all(14),
            padding: EdgeInsets.all(5),
            color: Colors.red,

            child: Text(article.source.name),
          ),
          SizedBox(
            height: 8.0,
          ),
          Container(
            margin: EdgeInsets.all(14),
            padding: EdgeInsets.all(5),
            width: double.infinity,
            child: Text(article.description,style: TextStyle(
              fontSize: 18,fontWeight:FontWeight.bold
            ),),
          ),
        ],
      ) ,
    );

  }
}
