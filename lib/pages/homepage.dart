import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../components/customlisttile.dart';
import '../model/articles.dart';
import '../services/api-services.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ApiServicesEndPoint client = ApiServicesEndPoint();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "NewsApp",
          style: TextStyle(fontSize: 24, color: Colors.white),
        ),
        backgroundColor: Colors.green,
      ),
      body: FutureBuilder(
        future: client.getArticles(),
        builder:
            (BuildContext context, AsyncSnapshot<List<Articles>> snapshot) {
    //(BuildContext context,List<Articles> snapshot) {
          if (snapshot.hasData) {
            List<Articles> articles = snapshot.requireData;
            print(articles);
            return ListView.builder(
              itemCount: articles.length,
              itemBuilder: (context, index) => customlisttile
                (articles[index],context),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
