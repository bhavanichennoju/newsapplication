import 'dart:convert';

import 'package:http/http.dart';
import 'package:newsapp/model/articles.dart';

class ApiServicesEndPoint{
  final  endPointUrl=
  "https://newsapi.org/v2/everything?q=apple&from=2022-05-01&to=2022-05-01&sortBy=popularity&apiKey=113f5ce23a164791b434ffc4509b63ea";


  Future<List<Articles>> getArticles() async{
    Response response=await get(endPointUrl);

    if(response.statusCode==200) {
      Map<String, dynamic> json = jsonDecode(response.body);
      List<dynamic> body = json['articles'];
      List<Articles> articles = body.map((dynamic item) =>
          Articles.fromJson(item)).toList();
      return articles;
    }
      else{
      throw ("Can't get the Articles");
  }
    }

}